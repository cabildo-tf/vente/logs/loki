# Loki

# Instalación de loki plugin
Para que el entorno de docker envíen los logs directamente al loki es necesario instalar
un plugin en los servidores que ejecutan un nodo swarm.

```bash
docker plugin install grafana/loki-docker-driver:latest --alias loki --grant-all-permissions
```

Además hay que añadir la siguiente configuración en el fichero /etc/docker/daemon.json, reemplazando
las variables necesarias.

```json
{
        "experimental": true,
        "metrics-addr": "0.0.0.0:9323",
        "debug": false,
        "log-driver": "loki",
        "log-opts": {
                "loki-url": "http://<url>:3100/loki/api/v1/push",
                "loki-tls-insecure-skip-verify": "true",
                "no-file": "false",
                "keep-file": "true",
                "max-size": "5m",
                "max-file": "1"
        }
}
```

Por último, es necesario reiniciar el servicio de docker.

```bash
sudo systemctl restart docker
```
